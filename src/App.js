import "./App.css";
import Form from "./Components/Form";
function App() {
  return (
    <div className='formContainer'>
      <Form />
    </div>
  );
}

export default App;
