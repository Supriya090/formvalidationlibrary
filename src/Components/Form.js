import React from "react";
import "../App.css";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const schema = yup.object().shape({
  username: yup.string().required("Username required"),
  email: yup.string().email().required("Email required"),
  password: yup.string().min(6).max(15).required("Password required"),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref("password"), null])
    .required("Password required"),
});

function Form() {
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const submitForm = (data) => {
    console.log(data);
  };
  return (
    <div className='formContent'>
      <form onSubmit={handleSubmit(submitForm)} className='form'>
        <h1>Sign Up</h1>
        <div className='formInputs'>
          <label htmlFor='username' className='formLabel'>
            Username
          </label>
          <input
            type='text'
            name='username'
            ref={register}
            className='formInput'
            placeholder='Enter your Username'
          />
          <p> {errors.username?.message} </p>
        </div>
        <div className='formInputs'>
          <label htmlFor='username' className='formLabel'>
            Email
          </label>
          <input
            type='text'
            name='email'
            className='formInput'
            placeholder='Enter your Email'
            ref={register}
          />
          <p> {errors.email?.message} </p>
        </div>
        <div className='formInputs'>
          <label htmlFor='username' className='formLabel'>
            Password
          </label>
          <input
            type='password'
            name='password'
            className='formInput'
            placeholder='Enter your password'
            ref={register}
          />
          <p> {errors.password?.message} </p>
        </div>
        <div className='formInputs'>
          <label htmlFor='username' className='formLabel'>
            Confirm Password
          </label>
          <input
            type='password'
            name='confirmPassword'
            className='formInput'
            placeholder='Confirm your password'
            ref={register}
          />
          <p>
            {Object.keys(errors).length
              ? Object.keys(errors.confirmPassword).length
                ? errors.confirmPassword.type === "required"
                  ? "Confirmation Password Required"
                  : "Password doesn't match"
                : ""
              : ""}
          </p>
          {Object.keys(errors).length
            ? console.log(errors.confirmPassword.type)
            : console.log("No errors")}
        </div>
        <button className='formInputButton' type='submit'>
          {" "}
          Sign Up
        </button>
      </form>
    </div>
  );
}

export default Form;
